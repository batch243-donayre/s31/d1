//Use the "require" directive to load the HTTP module of node JS
//A "module" is a software component or part of a program that contains one or more routines
//The 'http module' lets Node.js transfer data using the HTTP (HyperText Transfer Protocol)
//HTTP is a protocol that allows the fetching of resources such as HTML documents
//Clients (browser) and servers(nodeJS/ExpressJS applications) communicate by exchanging individual messages
//Set ofindividual file the compose set of components


//REQUEST -messages sent by the client (usually in a Web browser)
//RESPONSES - messages sent by tech server as an answer to the client


let http = require("http")

//CREATE A SERVER
	/*
	-The http module has a createServer() method that accepts a function as an arguments and allows server creation.

	-The arguments passed in the function are request && response objects (data type) that contains methods that allow us to receive request from the client and send the responses back

	-Using the module's createServer() method, we can create an HTTP server that listens to the request on a apecififed port and gives back to the client	
	*/

http.createServer(function(request,response){})


//Define the Port number that the server will be listening to
http.createServer(function(request,response){
	response.writeHead(200,{'Content-Type': 'Text/plain'});
	response.end('Hello World')
}).listen(4000)

console.log('Server running at localhost:4000')

//A port is virtual point where network connections start and end 

//Send a response back to the client
//Use the writeHead() method
//Set a status code for the response - a 200 - OK

//Inputting the command tells our device to run node js
//node index.js

//Install nodemon via terminal
//npm install -g nodemon

//To check if nodemon is already installed via terminal
//nodemon -v

/*
	IMPORTANT NOTE:
	Installing the package will alow the server toautomatically restart when files have been chnaged or updated
	"-g" refers to a global install where the current version of the 
*/
