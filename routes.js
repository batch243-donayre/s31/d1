/*
CREATE A ROUTE
	"/greeting"
*/

const http = require("http")

let url = require("url")

//
const port = 4000

const server = http.createServer((request,response)=>{

	//Creates a variable 'server' that stores the output of the 'createServer' method
	if(request.url == '/greeting'){
		response.writeHead(200,{'Content-Type' : 'text/plain'})
		response.end('Hello Again')

	//Accessing the 'homepage' route returns a message of 'hello world'
	}else if(request.url == '/hompage'){

		response.writeHead(200,{'Content-Type' : 'text/plain'})
		response.end('Welcome home')

	//All other routes will return a message of 'Page not available'
	}else{
		response.writeHead(200,{'Content-Type' : 'text/plain'})
		response.end('Page not available')
	}

})

//uses the 'server' and 'port' variables created above
server.listen(port)
console.log(`Server now accessible at localhost: ${port}`)